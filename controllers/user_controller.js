const crypto = require("crypto");
const path = require("path");
const { user, tickets } = require("../models");
passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;

class UserController {
  async bookTheMovie(req, res, next) {
    try {
      let userBooking = await tickets.create(req.body);
      let data = await user.findOneAndUpdate(
        { _id: req.user.id },
        { tickets: userBooking._id },
        {
          new: true,
        }
      );
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async getUserInfo(req, res, next) {
    try {
      let userInfo = await user
        .findOne({ _id: req.params.id })
        .populate("tickets")
        .exec(function (error, result) {
          if (error) return next(error);
          return res.status(200).json({
            message: "Success",
            result,
          });
        });
      // return res.status(200).json({
      //   message: "Success",
      //   userInfo,
      // });
    } catch (error) {
      return next(error);
    }
  }
  async signup(req, res, next) {
    try {
      passport.authenticate("signup", { session: false }, (err, user, info) => {
        if (err) return next(err);
        if (!user) return next({ message: info.message, statusCode: 401 });
        req.user = user;
        next();
      })(req, res, next);
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
      });
    }
  }
}
passport.use(
  "signup",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        let userSignup = await user.create(req.body);
        return done(null, userSignup, {
          message: "Signup Success",
        });
      } catch (error) {
        return done(null, false, {
          // message: "something wrong while signup",
          message: error.message,
        });
      }
    }
  )
);
module.exports = new UserController();
