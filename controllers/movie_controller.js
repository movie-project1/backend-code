const crypto = require("crypto");
const path = require("path");
const { movie, genres } = require("../models");
const moment = require("moment");

class MovieController {
  async create(req, res, next) {
    try {
      const duplicationCheck = await movie.findOne({ title: req.body.title });
      if (duplicationCheck == null) {
        let userData = await movie.create(req.body);
        let data = await movie.findOne({ _id: userData._id });
        return res.status(201).json({
          message: "Success",
          data,
        });
      } else {
        return next({
          message: `${duplicationCheck.title} is already in our database`,
          statusCode: 409,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async showMovie(req, res, next) {
    try {
      await movie
        .findOne({ _id: req.params.id })
        .populate("movie_genre")
        .exec(function (error, data) {
          if (error) return next(error);
          return res.status(200).json({
            message: "Success",
            data,
          });
        });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async showMovies(req, res) {
    try {
      await movie
        .find({})
        .populate("movie_genre")
        .exec(function (error, data) {
          if (error) return next(error);
          return res.status(200).json({
            message: "Success",
            data,
          });
        });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async sortMoviesByParams(req, res, next) {
    try {
      const sortparams = req.query.sortParams;
      const sortBy = req.query.sortBy;
      let data = await movie
        .find({})
        .populate("movie_genre")
        .sort([[`${sortBy}`, `${sortparams}`]]);
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return next(error);
    }
  }
  async searchMovie(req, res, next) {
    try {
      const sortParams = req.query.sortParams;
      const keywords = req.query.keywords;
      let sortBy = req.query.sortBy;
      if (sortBy.length <= 0) {
        sortBy = "title";
      }
      let data = await movie
        .find({ title: { $regex: keywords } })
        .populate("movie_genre")
        .sort([[`${sortBy}`, `${sortParams}`]]);
      if (data.length <= 0) {
        return res.status(404).json({
          message: `No Movie found with title ${keywords}`,
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return next(error);
    }
  }
  async updateMovie(req, res, next) {
    try {
      let data = await movie.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async deleteMovie(req, res) {
    try {
      await movie.deleteOne({ _id: req.params.id });
      return res.status(200).json({
        message: "Success",
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
}

module.exports = new MovieController();
