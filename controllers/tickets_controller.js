const crypto = require("crypto");
const path = require("path");
const { tickets } = require("../models");

class TicketsController {
  async create(req, res) {
    try {
      let userData = await tickets.create(req.body);
      let data = await tickets.findOne({});
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        message: "Internal Server Error",
        error: error,
      });
    }
  }
}

module.exports = new TicketsController();
