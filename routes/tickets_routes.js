const express = require("express");
const router = express.Router();

//Middleware
const ticketsValidator = require("../middleware/validators/tickets_validator");

//Controller
const ticketsController = require("../controllers/tickets_controller");

router.post("/", ticketsValidator.create, ticketsController.create);
module.exports = router;
