const express = require("express");
const router = express.Router();

//Middleware
const genresValidator = require("../middleware/validators/genres_validator");

//Controller
const genresController = require("../controllers/genres_controller");

router.post("/", genresValidator.create, genresController.create);
module.exports = router;
