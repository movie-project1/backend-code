const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
const moment = require("moment");

const MovieSchema = new mongoose.Schema(
  {
    title: { type: String, required: true, unique: true },
    description: { type: String, required: true },
    ratings: { type: Number, required: false },
    status: { type: Number, required: false },
    todayBook: { type: Number, required: false },
    totalBook: { type: Number, required: false },
    movie_genre: [
      {
        type: [mongoose.Schema.Types.ObjectId],
        ref: "genres",
        required: false,
      },
    ],
    poster: {
      type: String,
      get: getPoster,
      required: false,
    },
    booking_time: { date: [Date] },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      deletedAt: "deletedAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);
function getPoster(poster) {
  if (!poster) return poster;
  return `${process.env.S3_HOST}/${poster}`;
}
//Enable Soft Delete
MovieSchema.plugin(mongooseDelete, { overridemethods: "all" });
module.exports = mongoose.model("movie", MovieSchema, "movie");
