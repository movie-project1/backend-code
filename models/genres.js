const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const GenresSchema = new mongoose.Schema({
  genreName: { type: String, required: true, unique: true },
});
//Enable Soft Delete
GenresSchema.plugin(mongooseDelete, { overridemethods: "all" });
module.exports = mongoose.model("genres", GenresSchema, "genres");
