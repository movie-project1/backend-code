const mongoose = require("mongoose");
const validator = require("validator");
const { user } = require("../../models");

exports.signup = async (req, res, next) => {
  try {
    req.body.directory = "User-Poster";
    let userCheck = await user.findOne({ email: req.body.email });
    if (userCheck)
      return res.status(400).json({
        message: `${req.body.email} already in our database`,
      });
    else next();
  } catch (error) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: error,
    });
  }
};
