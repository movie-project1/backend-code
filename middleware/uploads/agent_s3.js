const path = require("path");
const crypto = require("crypto");
const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");

const AWS_REGION = "ap-southeast-1";

const s3Uploadparams = (directory, filename, body, mimetype) => {
  const upload = {
    ACL: "public-read",
    Bucket: process.env.S3_BUCKET,
    Key: `${directory}/${filename}`,
    Body: body,
    ContentType: mimetype,
  };
  return upload;
};
const clientS3 = new S3Client({
  region: AWS_REGION,
  credentials: {
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY,
  },
});
const uploading = async (directory, filename, body, mimetype) => {
  try {
    const data = await clientS3.send(
      new PutObjectCommand(s3Uploadparams(directory, filename, body, mimetype))
    );
    return directory + "/" + filename;
  } catch (error) {
    console.log("error: ", error);
  }
};

exports.sendToS3 = async (directory, file) => {
  try {
    let fileName = crypto.randomBytes(16).toString("hex");
    file.name = `${fileName}${path.parse(file.name).ext}`;
    //uploading to aws directory
    let image = await uploading(directory, file.name, file.data, file.mimetype);
    return image;
  } catch (error) {
    return res.status(500).json({
      message: error.message,
    });
  }
};
